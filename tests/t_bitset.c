/*-
 * Copyright (c) 2018, 2021 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

#include "t_trie_defs.h"

#include "bitset_builder.h"

#include <limits.h>
#include <paths.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

static inline FILE *
create_temporary_file()
{
	char path[PATH_MAX];
	int fd;
	FILE *fp;

	ATF_CHECK(strlcpy(path, _PATH_TMP, sizeof(path)) < sizeof(path));
	ATF_CHECK(strlcat(path, "/", sizeof(path)) < sizeof(path));
	ATF_CHECK(strlcat(path, getprogname(), sizeof(path)) < sizeof(path));
	ATF_CHECK(strlcat(path, ".XXXXXX", sizeof(path)) < sizeof(path));
	fd = mkstemp(path);
	ATF_CHECK(fd != -1);
	ATF_CHECK(unlink(path) == 0);
	fp = fdopen(fd, "w+");
	ATF_CHECK(fp != NULL);
	return fp;
}

ATF_TC(test_bitset_zero_fill);
ATF_TC_HEAD(test_bitset_zero_fill, tc)
{
	atf_tc_set_md_var(tc, "descr", "test_bitset_zero_fill");
}
ATF_TC_BODY(test_bitset_zero_fill, tc)
{
	bitset_t bs;
	size_t i;
	FILE *fp;

	ATF_CHECK((bs = bitset_new()) != NULL);
	for (i = 0; i < 2048; ++i)
		ATF_CHECK(bitset_push_back(bs, false) == 0);
	ATF_CHECK(bitset_size(bs) == 2048);
	for (i = 0; i < bitset_size(bs); ++i)
		ATF_CHECK(bitset_get_value(bs, i) == false);
	for (i = 0; i < bitset_size(bs); ++i)
		bitset_set_value(bs, i, true);
	for (i = 0; i < bitset_size(bs); ++i)
		ATF_CHECK(bitset_get_value(bs, i) == true);
	fp = create_temporary_file();
	ATF_CHECK(bitset_store(bs, fp) == 0);
	bitset_delete(bs);
	rewind(fp);
	ATF_CHECK(bitset_load(fp, &bs) == 0);
	fclose(fp);
	for (i = 0; i < bitset_size(bs); ++i)
		ATF_CHECK(bitset_get_value(bs, i) == true);
	bitset_delete(bs);
}

ATF_TC(test_bitset_one_fill);
ATF_TC_HEAD(test_bitset_one_fill, tc)
{
	atf_tc_set_md_var(tc, "descr", "test_bitset_one_fill");
}
ATF_TC_BODY(test_bitset_one_fill, tc)
{
	bitset_t bs;
	size_t i;
	FILE *fp;

	ATF_CHECK((bs = bitset_new()) != NULL);
	for (i = 0; i < 2048; ++i)
		ATF_CHECK(bitset_push_back(bs, true) == 0);
	ATF_CHECK(bitset_size(bs) == 2048);
	for (i = 0; i < bitset_size(bs); ++i)
		ATF_CHECK(bitset_get_value(bs, i) == true);
	for (i = 0; i < bitset_size(bs); ++i)
		bitset_set_value(bs, i, false);
	for (i = 0; i < bitset_size(bs); ++i)
		ATF_CHECK(bitset_get_value(bs, i) == false);
	fp = create_temporary_file();
	ATF_CHECK(bitset_store(bs, fp) == 0);
	bitset_delete(bs);
	rewind(fp);
	ATF_CHECK(bitset_load(fp, &bs) == 0);
	fclose(fp);
	for (i = 0; i < bitset_size(bs); ++i)
		ATF_CHECK(bitset_get_value(bs, i) == false);
	bitset_delete(bs);
}

ATF_TC(test_bitset_random_fill);
ATF_TC_HEAD(test_bitset_random_fill, tc)
{
	atf_tc_set_md_var(tc, "descr", "test_bitset_random_fill");
}
ATF_TC_BODY(test_bitset_random_fill, tc)
{
	bitset_t bs;
	bool testcase[2048];
	size_t i;
	FILE *fp;

	ATF_CHECK((bs = bitset_new()) != NULL);
	for (i = 0; i < arraycount(testcase); ++i) {
		testcase[i] = (bool)arc4random_uniform(2);
		ATF_CHECK(bitset_push_back(bs, testcase[i]) == 0);
	}
	ATF_CHECK(bitset_size(bs) == arraycount(testcase));
	for (i = 0; i < bitset_size(bs); ++i)
		ATF_CHECK(bitset_get_value(bs, i) == testcase[i]);
	for (i = 0; i < bitset_size(bs); ++i)
		bitset_set_value(bs, i, !testcase[i]);
	for (i = 0; i < bitset_size(bs); ++i)
		ATF_CHECK(bitset_get_value(bs, i) != testcase[i]);
	fp = create_temporary_file();
	ATF_CHECK(bitset_store(bs, fp) == 0);
	bitset_delete(bs);
	rewind(fp);
	ATF_CHECK(bitset_load(fp, &bs) == 0);
	fclose(fp);
	for (i = 0; i < bitset_size(bs); ++i)
		ATF_CHECK(bitset_get_value(bs, i) != testcase[i]);
	bitset_delete(bs);
}

ATF_TP_ADD_TCS(tp)
{
	ATF_TP_ADD_TC(tp, test_bitset_zero_fill);
	ATF_TP_ADD_TC(tp, test_bitset_one_fill);
	ATF_TP_ADD_TC(tp, test_bitset_random_fill);

	return atf_no_error();
}
