/*-
 * Copyright (c) 2018, 2021 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

#include "t_trie_defs.h"

#include "string_array_builder.h"
#include "string_array_local.h"

#include <limits.h>
#include <paths.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <uuid.h>

static inline FILE *
create_temporary_file()
{
	char path[PATH_MAX];
	int fd;
	FILE *fp;

	ATF_CHECK(strlcpy(path, _PATH_TMP, sizeof(path)) < sizeof(path));
	ATF_CHECK(strlcat(path, "/", sizeof(path)) < sizeof(path));
	ATF_CHECK(strlcat(path, getprogname(), sizeof(path)) < sizeof(path));
	ATF_CHECK(strlcat(path, ".XXXXXX", sizeof(path)) < sizeof(path));
	fd = mkstemp(path);
	ATF_CHECK(fd != -1);
	ATF_CHECK(unlink(path) == 0);
	fp = fdopen(fd, "w+");
	ATF_CHECK(fp != NULL);
	return fp;
}

ATF_TC(test_string_array);
ATF_TC_HEAD(test_string_array, tc)
{
	atf_tc_set_md_var(tc, "descr", "test_string_array");
}
ATF_TC_BODY(test_string_array, tc)
{
	string_array_builder_t sab;
	size_t i, src_size, dst_size;
	char *tests[STRNG_ARRAY_BUILDER_DEFAULT_CAPACITY * 2], **dst;
	const char **src, *s;
	FILE *fp;
	uuid_t uuid;

	for (i = 0; i < arraycount(tests); ++i) {
		uuid_create(&uuid, NULL);
		uuid_to_string(&uuid, &tests[i], NULL);
	}
	sab = string_array_builder_new(&src, &src_size);
	for (i = 0; i < arraycount(tests); ++i) {
		s = (const char *)tests[i];
		ATF_CHECK(string_array_builder_append(sab, s) == s);
	}
	ATF_CHECK(string_array_builder_shrink_to_fit(sab) == 0);
	ATF_CHECK(src_size == arraycount(tests));
	ATF_CHECK(sab->capacity == src_size + 1);
	string_array_builder_delete(sab);
	for (i = 0; i < src_size; ++i)
		ATF_CHECK(src[i] == tests[i]);
	ATF_CHECK(src[src_size] == NULL);
	fp = create_temporary_file();
	ATF_CHECK(string_array_store(src, src_size, fp) == 0);
	rewind(fp);
	ATF_CHECK(string_array_load(fp, &dst, &dst_size) == 0);
	fclose(fp);
	ATF_CHECK(dst_size == src_size);
	for (i = 0; i < src_size; ++i)
		ATF_CHECK(!strcmp(src[i], dst[i]));
	for (i = 0; i < arraycount(tests); ++i)
		free(tests[i]);
	free(src);
	for (i = 0; i < dst_size; ++i)
		free(dst[i]);
	free(dst);
}

ATF_TP_ADD_TCS(tp)
{
	ATF_TP_ADD_TC(tp, test_string_array);

	return atf_no_error();
}
