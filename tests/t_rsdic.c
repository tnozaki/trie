/*-
 * Copyright (c) 2021 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

#include "t_trie_defs.h"

#include "bitset_builder.h"
#include "rsdic_builder.h"

#include <limits.h>
#include <paths.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

static inline FILE *
create_temporary_file()
{
	char path[PATH_MAX];
	int fd;
	FILE *fp;

	ATF_CHECK(strlcpy(path, _PATH_TMP, sizeof(path)) < sizeof(path));
	ATF_CHECK(strlcat(path, "/", sizeof(path)) < sizeof(path));
	ATF_CHECK(strlcat(path, getprogname(), sizeof(path)) < sizeof(path));
	ATF_CHECK(strlcat(path, ".XXXXXX", sizeof(path)) < sizeof(path));
	fd = mkstemp(path);
	ATF_CHECK(fd != -1);
	ATF_CHECK(unlink(path) == 0);
	fp = fdopen(fd, "w+");
	ATF_CHECK(fp != NULL);
	return fp;
}

static inline void
verify(rsdic_t rd, bool *b, size_t n)
{
	size_t len, i, r0, r1;

	ATF_CHECK((len = rsdic_get_size(rd)) == n);
	r0 = r1 = 0;
	for (i = 0; i < len; ++i) {
		ATF_CHECK(rsdic_get_value(rd, i) == b[i]);
		if (b[i]) {
			++r0;
			ATF_CHECK(r0 == rsdic_rank(rd, i, true));
			ATF_CHECK(i == rsdic_select(rd, r0, true));
		} else {
			++r1;
			ATF_CHECK(r1 == rsdic_rank(rd, i, false));
			ATF_CHECK(i == rsdic_select(rd, r1, false));
		}
	}
}

ATF_TC(test_rsdic_random_fill);
ATF_TC_HEAD(test_rsdic_random_fill, tc)
{
	atf_tc_set_md_var(tc, "descr", "test_rsdic_random_fill");
}
ATF_TC_BODY(test_rsdic_random_fill, tc)
{
	bitset_t bs;
	bool testcase[2048];
	size_t i;
	rsdic_t rd;
	FILE *fp;

	ATF_CHECK((bs = bitset_new()) != NULL);
	ATF_CHECK(bs != NULL);
	for (i = 0; i < arraycount(testcase); ++i) {
		testcase[i] = (bool)arc4random_uniform(2);
		ATF_CHECK(bitset_push_back(bs, testcase[i]) == 0);
	}
	ATF_CHECK((rd = rsdic_new(bs)) != NULL);
	bitset_delete(bs);
	verify(rd, testcase, arraycount(testcase));
	fp = create_temporary_file();
        ATF_CHECK(rsdic_store(rd, fp) == 0);
	rsdic_delete(rd);
	rewind(fp);
	ATF_CHECK(rsdic_load(fp, &rd) == 0);
	fclose(fp);
	verify(rd, testcase, arraycount(testcase));
	rsdic_delete(rd);
}

ATF_TP_ADD_TCS(tp)
{
	ATF_TP_ADD_TC(tp, test_rsdic_random_fill);

	return atf_no_error();
}
