/*-
 * Copyright (c) 2014, 2018, 2021 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

#include "trie_defs.h"

#include "byte_array_builder.h"
#include "byte_array_local.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>

byte_array_builder_t
byte_array_builder_new(char **bufp, size_t *sizep)
{
	byte_array_builder_t p;
	char *tmp;

	p = malloc(sizeof(*p));
	if (p != NULL) {
		p->capacity = BYTE_ARRAY_BUILDER_DEFAULT_CAPACITY;
		tmp = calloc(p->capacity, sizeof(*tmp));
		if (tmp != NULL) {
			*(p->bufp = bufp) = tmp;
			*(p->sizep = sizep) = 0;
			return p;
		}
		free(p);
	}
	return NULL;
}

void
byte_array_builder_delete(byte_array_builder_t p)
{
	free(p);
}

int
byte_array_builder_append(byte_array_builder_t p, int c)
{
	size_t size, capacity;
	char *tmp;

	size = *p->sizep;
	if (p->capacity - 1 == size) {
		if (BYTE_ARRAY_BUILDER_MAX_CAPACITY / 2 < p->capacity)
			return EOF;
		capacity = p->capacity * 2;
		tmp = realloc(*p->bufp, capacity);
		if (tmp == NULL)
			return EOF;
		memset(&tmp[size], 0, capacity - size);
		*p->bufp = tmp;
		p->capacity = capacity;
	} else {
		tmp = *p->bufp;
	}
	tmp[size++] = c;
	*p->sizep = size;
	return c;
}

int
byte_array_builder_shrink_to_fit(byte_array_builder_t p)
{
	size_t capacity;
	char *tmp;

	capacity = *p->sizep + 1;
	tmp = realloc(*p->bufp, capacity);
	if (tmp == NULL)
		return 1;
	*p->bufp = tmp;
	p->capacity = capacity;
	return 0;
}

int
byte_array_serialize(const char *data, size_t size, FILE *fp)
{
	struct byte_array_file baf;

	assert(data != NULL);
	assert(fp != NULL);
	if (size < 1 || size > UINT64_MAX)
		return 1;
	baf.size = htobe64((uint64_t)size);
	if (fwrite(&baf, sizeof(baf), 1, fp) != 1)
		return 1;
	if (fwrite(data, sizeof(*data), size, fp) != size)
		return 1;
	return 0;
}

int
byte_array_store(const char *data, size_t size, FILE *fp)
{
	struct byte_array_header bah;

	assert(data != NULL);
	assert(fp != NULL);
	memcpy(&bah.magic[0],
	    BYTE_ARRAY_HEADER_MAGIC, sizeof(bah.magic));
	bah.version = htobe32(BYTE_ARRAY_HEADER_VERSION);
	if (fwrite(&bah, sizeof(bah), 1, fp) != 1)
		return 1;
	return byte_array_serialize(data, size, fp);
}
