/*-
 * Copyright (c) 2018, 2021 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

#include "trie_defs.h"

#include "rsdic.h"
#include "rsdic_local.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#define SIEVE(x, mask, bit) ((x & mask) + (x >> bit & mask))

static inline int
select32(uint32_t x, int n)
{
	uint32_t x1, x2, x3;
	int rank, ret;

	x1 = x - ((x & UINT32_C(0xaaaaaaaa)) >> 1);
	x2 = SIEVE(x1, UINT32_C(0x33333333), 2);
	x3 = SIEVE(x2, UINT32_C(0x0f0f0f0f), 4);
	for (ret = 0; /**/; ret += 8) {
		rank = (x3 >> ret) & UINT32_C(0xff);
		if (n <= rank)
			break;
		n -= rank;
	}
	rank = (x2 >> ret) & UINT32_C(0xf);
	if (n > rank) {
		n -= rank;
		ret += 4;
	}
	rank = (x1 >> ret) & UINT32_C(0x3);
	if (n > rank) {
		n -= rank;
		ret += 2;
	}
	rank = (x  >> ret) & UINT32_C(0x1);
	if (n > rank) {
		n -= rank;
		++ret;
	}
	return ret;
}

int
rsdic_deserialize(FILE *fp, rsdic_t *rdp)
{
	struct rsdic_file rdf;
	rsdic_t rd;
	size_t size, alloc_size, block_size, rank_size, i;

	assert(fp != NULL);
	assert(rdp != NULL);
	if (fread(&rdf, sizeof(rdf), 1, fp) != 1)
		return 1;
	rdf.size = be64toh(rdf.size);
	if (rdf.size < 1 || rdf.size > SIZE_MAX)
		return 1;
	size = (size_t)rdf.size;
	alloc_size = sizeof(*rd);
	block_size = (size / 32) + ((size % 32) ? 1 : 0);
	alloc_size += block_size;
	rank_size = (block_size / 8) + ((block_size % 8) ? 1 : 0) + 1;
	alloc_size += rank_size;
	rd = malloc(alloc_size);
	if (rd == NULL)
		return 1;
	rd->size = size;
	rd->block_size = block_size;
	rd->rank_size = rank_size;
	rd->block = (uint32_t *)(void *)(rd + 1);
	rd->rank = rd->block + block_size;
	block_size += rank_size;
	if (fread(rd->block, sizeof(*rd->block), block_size, fp)
	  != block_size) {
		free(rd);
		return 1;
	}
	for (i = 0; i < block_size; ++i)
		rd->block[i] = be32toh(rd->block[i]);
	*rdp = rd;
	return 0;
}

int
rsdic_load(FILE *fp, rsdic_t *rdp)
{
	struct rsdic_header rdh;

	assert(fp != NULL);
	assert(rdp != NULL);
	if (fread(&rdh, sizeof(rdh), 1, fp) != 1)
		return 1;
	if (memcmp(&rdh.magic[0],
	    RSDIC_HEADER_MAGIC, sizeof(rdh.magic)) != 0)
		return 1;
	if (be32toh(rdh.version) != RSDIC_HEADER_VERSION)
		return 1;
	return rsdic_deserialize(fp, rdp);
}

void
rsdic_delete(rsdic_t rd)
{
	assert(rd != NULL);
	free(rd);
}

size_t
rsdic_get_size(rsdic_t rd)
{
	assert(rd != NULL);
	return rd->size;
}

bool
rsdic_get_value(rsdic_t rd, size_t pos)
{
	assert(rd != NULL);
	return rd->block[pos / 32] & UINT32_C(1) << (pos % 32);
}

size_t
rsdic_rank(rsdic_t rd, size_t pos, bool bit)
{
	size_t rank, left, right, i, n;
	uint32_t mask;

	assert(rd != NULL);
	assert(pos < rd->size);
	rank = (size_t)rd->rank[++pos >> 8];
	left = (pos >> 8) << 3;
	right = pos >> 5;
	assert(left <= rd->block_size);
	assert(right <= rd->block_size);
	for (i = left; i < right; ++i) {
		n = (size_t)popcount32(rd->block[i]);
		assert(rank <= SIZE_MAX - n);
		rank += n;
	}
	mask = (1 << (int)(pos % 32)) - 1;
	n = (size_t)popcount32(rd->block[right] & mask);
	assert(rank <= SIZE_MAX - n);
	rank += n;
	if (bit)
		return rank;
	assert(rank <= pos);
	return pos - rank;
}

size_t
rsdic_select(rsdic_t rd, size_t pos, bool bit)
{
	size_t i, lim, mid, rank, n;
	uint32_t block;

	assert(rd != NULL);
	i = 0;
	lim = rd->rank_size;
	while (i < lim) {
		mid = i + (lim - i) / 2;
		rank = (size_t)rd->rank[mid];
		n = bit ? rank : mid * 256 - rank;
		if (n < pos)
			i = mid + 1;
		else
			lim = mid;
	}
	if (i > 0)
		--i;
	rank = (size_t)rd->rank[i];
	n = bit ? rank : i * 256 - rank;
	assert(n <= pos);
	pos -= n;
	lim = rd->block_size;
	assert(i <= (SIZE_MAX - lim) / 8);
	for (i *= 8; i < lim; ++i) {
		block = bit ? rd->block[i] : ~rd->block[i];
		n = (size_t)popcount32(block);
		if (n >= pos)
			break;
		pos -= n;
	}
	block = bit ? rd->block[i] : ~rd->block[i];
	n = (size_t)select32(block, (int)pos);
	assert(i <= (SIZE_MAX - n) / 32);
	return i * 32 + n;
}
