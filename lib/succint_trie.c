/*-
 * Copyright (c) 2018, 2021 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

#include "trie_defs.h"

#include "byte_array.h"
#include "string_array.h"
#include "succint_trie.h"
#include "succint_trie_local.h"

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int
succint_trie_deserialize(FILE *fp, succint_trie_t *stp)
{
	succint_trie_t st;
        struct succint_trie_file stf;

	assert(fp != NULL);
	assert(stp != NULL);
	st = malloc(sizeof(*st));
	if (st == NULL)
		return 1;
	memset(st, 0, sizeof(*st));
	if (fread(&stf, sizeof(stf), 1, fp) != 1)
		goto fatal;
	stf.key_num = be64toh(stf.key_num);
	if (stf.key_num < 1 || stf.key_num > SIZE_MAX)
		goto fatal;
	st->key_num = (size_t)stf.key_num;
	if (rsdic_deserialize(fp, &st->loud)
	 || rsdic_deserialize(fp, &st->term)
	 || rsdic_deserialize(fp, &st->tail)
	 || string_array_deserialize(fp, &st->vtails, &st->vtails_size)
	 || byte_array_deserialize(fp, &st->edges, &st->edges_size))
		goto fatal;
	return 0;
fatal:
	succint_trie_delete(st);
	return 1;
}

int
succint_trie_load(FILE *fp, succint_trie_t *stp)
{
	struct succint_trie_header sth;

	assert(fp != NULL);
	assert(stp != NULL);
	if (fread(&sth, sizeof(sth), 1, fp) != 1)
		return 1;
	if (memcmp(&sth.magic[0],
	    SUCCINT_TRIE_HEADER_MAGIC, sizeof(sth.magic)) != 0)
		return 1;
	if (be32toh(sth.version) != SUCCINT_TRIE_HEADER_VERSION)
		return 1;
	return succint_trie_deserialize(fp, stp);
}

void
succint_trie_delete(succint_trie_t st)
{
	size_t i;

	if (st->loud != NULL)
		rsdic_delete(st->loud);
	if (st->term != NULL)
		rsdic_delete(st->term);
	if (st->tail != NULL)
		rsdic_delete(st->tail);
	for (i = 0; i < st->vtails_size; ++i)
		free(st->vtails[i]);
	free(st->vtails);
	free(st);
}
