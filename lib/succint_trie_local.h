/*-
 * Copyright (c) 2018, 2021 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

#ifndef SUCCINT_TRIE_LOCAL_H_
#define SUCCINT_TRIE_LOCAL_H_

#include <stddef.h>
#include <stdint.h>
#include <bitset.h>
#include <rsdic.h>

#define SUCCINT_TRIE_HEADER_MAGIC	"succint_trie"
#define SUCCINT_TRIE_HEADER_VERSION	UINT32_C(1)

struct succint_trie_header {
	char magic[sizeof(SUCCINT_TRIE_HEADER_MAGIC) - 1];
	uint32_t version;
};

struct succint_trie_file {
	uint64_t key_num;
};

struct succint_trie {
	size_t key_num;
	rsdic_t loud;
	rsdic_t term;
	rsdic_t tail;
	size_t vtails_size;
	char **vtails;
	size_t edges_size;
	char *edges;
};

#endif /*!SUCCINT_TRIE_LOCAL_H_*/
