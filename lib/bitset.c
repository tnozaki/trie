/*-
 * Copyright (c) 2018, 2021 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

#include "trie_defs.h"

#include "bitset.h"
#include "bitset_local.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>

int
bitset_deserialize(FILE *fp, struct bitset **bsp)
{
	struct bitset_file bsf;
	struct bitset *bs;
	size_t i;

	assert(bsp != NULL);
	if (fread(&bsf, sizeof(bsf), 1, fp) != 1)
		return 1;
	bs = malloc(sizeof(*bs));
	if (bs == NULL)
		return 1;
	bsf.size = be64toh(bsf.size);
	if (bsf.size < 1 || bsf.size > SIZE_MAX)
		return 1;
	bs->size = (size_t)bsf.size;
	bs->block_size = (bs->size / 32) + ((bs->size % 32) ? 1 : 0);
	bs->block = malloc(bs->block_size * sizeof(*bs->block));
	if (bs->block == NULL) {
		free(bs);
		return 1;
	}
	if (fread(bs->block, sizeof(*bs->block), bs->block_size, fp) !=
	    bs->block_size) {
		free(bs->block);
		free(bs);
		return 1;
	}
	for (i = 0; i < bs->block_size; ++i)
		bs->block[i] = be32toh(bs->block[i]);
	*bsp = bs;
	return 0;
}

int
bitset_load(FILE *fp, struct bitset **bsp)
{
	struct bitset_header bsh;

	assert(fp != NULL);
	assert(bsp != NULL);
	if (fread(&bsh, sizeof(bsh), 1, fp) != 1)
		return 1;
	if (memcmp(&bsh.magic[0],
	    BITSET_HEADER_MAGIC, sizeof(bsh.magic)) != 0)
		return 1;
	if (be32toh(bsh.version) != BITSET_HEADER_VERSION)
		return 1;
	return bitset_deserialize(fp, bsp);
}

void
bitset_delete(struct bitset *bs)
{
	assert(bs != NULL);
	free(bs->block);
	free(bs);
}

bool
bitset_get_value(struct bitset *bs, size_t pos)
{
	assert(bs != NULL);
	return bs->block[pos / 32] & UINT32_C(1) << (pos % 32);
}

size_t
bitset_size(struct bitset *bs)
{
	assert(bs != NULL);
	return bs->size;
}
