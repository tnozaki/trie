/*-
 * Copyright (c) 2018, 2021 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

#include "trie_defs.h"

#include "byte_array.h"
#include "byte_array_local.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>

int
byte_array_deserialize(FILE *fp, char **datap, size_t *sizep)
{
	struct byte_array_file baf;
	size_t size;
	char *data;

	assert(fp != NULL);
	assert(datap != NULL);
	assert(sizep != NULL);
	if (fread(&baf, sizeof(baf), 1, fp) != 1)
		return 1;
	baf.size = be64toh(baf.size);
	if (baf.size < 1 || baf.size > SIZE_MAX)
		return 1;
	size = (size_t)baf.size;
	data = malloc(sizeof(*data) * size);
	if (data == NULL)
		return 1;
	if (fread(data, sizeof(*data), size, fp) != size) {
		free(data);
		return 1;
	}
	*datap = data;
	*sizep = size;
	return 0;
}

int
byte_array_load(FILE *fp, char **datap, size_t *sizep)
{
	struct byte_array_header bah;

	assert(fp != NULL);
	assert(datap != NULL);
	assert(sizep != NULL);
	if (fread(&bah, sizeof(bah), 1, fp) != 1)
		return 1;
	if (memcmp(&bah.magic[0],
	    BYTE_ARRAY_HEADER_MAGIC, sizeof(bah.magic)) != 0)
		return 1;
	if (be32toh(bah.version) != BYTE_ARRAY_HEADER_VERSION)
		return 1;
	return byte_array_deserialize(fp, datap, sizep);
}

void
byte_array_reverse(char *data, size_t size)
{
	char *tail;
	char c;

	assert(data != NULL);
	assert(size > 0);
	tail = data + size;
	while (data < --tail) {
		c = *data;
		*data++ = *tail;
		*tail = c;
	}
}
