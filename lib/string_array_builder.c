/*-
 * Copyright (c) 2014, 2018, 2021 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

#include "trie_defs.h"

#include "string_array_builder.h"
#include "string_array_local.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>

string_array_builder_t
string_array_builder_new(const char ***bufp, size_t *sizep)
{
	string_array_builder_t p;
	const char **tmp;

	p = malloc(sizeof(*p));
	if (p != NULL) {
		p->capacity = STRNG_ARRAY_BUILDER_DEFAULT_CAPACITY;
		tmp = calloc(p->capacity, sizeof(*tmp));
		if (tmp != NULL) {
			*(p->bufp = bufp) = tmp;
			*(p->sizep = sizep) = 0;
			return p;
		}
		free(p);
	}
	return NULL;
}

void
string_array_builder_delete(string_array_builder_t p)
{
	free(p);
}

const char *
string_array_builder_append(string_array_builder_t p, const char *s)
{
	size_t size, capacity;
	const char **tmp;

	size = *p->sizep;
	if (p->capacity - 1 == size) {
		if (STRNG_ARRAY_BUILDER_MAX_CAPACITY / 2 < p->capacity)
			return NULL;
		capacity = p->capacity * 2;
		tmp = realloc(*p->bufp, capacity * sizeof(*tmp));
		if (tmp == NULL)
			return NULL;
		memset(&tmp[size], 0, capacity - size);
		*p->bufp = tmp;
		p->capacity = capacity;
	} else {
		tmp = *p->bufp;
	}
	tmp[size++] = s;
	*p->sizep = size;
	return s;
}

int
string_array_builder_shrink_to_fit(string_array_builder_t p)
{
	size_t capacity;
	const char **tmp;

	capacity = *p->sizep + 1;
	tmp = realloc(*p->bufp, capacity * sizeof(*tmp));
	if (tmp == NULL)
		return EOF;
	*p->bufp = tmp;
	p->capacity = capacity;
	return 0;
}

int
string_array_serialize(const char **data, size_t size, FILE *fp)
{
	struct string_array_file saf;
	uint64_t u64;
	size_t i, n;
	const char *s;

	assert(data != NULL);
	assert(fp != NULL);
	if (size < 1 || size > UINT64_MAX)
		return 1;
	saf.size = htobe64((uint64_t)size);
	if (fwrite(&saf, sizeof(saf), 1, fp) != 1)
		return 1;
	for (i = 0; i < size; ++i) {
		s = data[i];
		if (s == NULL)
			return 1;
		n = strlen(s);
		if (n > UINT64_MAX)
			return 1;
		u64 = htobe64((uint64_t)n);
		if (fwrite(&u64, sizeof(u64), 1, fp) != 1)
			return 1;
		if (fwrite(s, sizeof(*s), n, fp) != n)
			return 1;
	}
	return 0;
}

int
string_array_store(const char **data, size_t size, FILE *fp)
{
	struct string_array_header sah;

	assert(data != NULL);
	assert(fp != NULL);
	memcpy(&sah.magic[0],
	    STRING_ARRAY_HEADER_MAGIC, sizeof(sah.magic));
	sah.version = htobe32(STRING_ARRAY_HEADER_VERSION);
	if (fwrite(&sah, sizeof(sah), 1, fp) != 1)
		return 1;
	return string_array_serialize(data, size, fp);
}
