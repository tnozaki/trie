/*-
 * Copyright (c) 2018, 2021 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

#include "trie_defs.h"

#include "rsdic_builder.h"
#include "rsdic_local.h"
#include "bitset_local.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>

rsdic_t
rsdic_new(bitset_t bs)
{
	rsdic_t rd;
	size_t size, alloc_size, block_size, rank_size, i;
	uint32_t rank, block;

	assert(bs != NULL);
	size = bs->size;
	alloc_size = sizeof(*rd);
	block_size = (size / 32) + ((size % 32) ? 1 : 0);
	alloc_size += block_size * sizeof(*rd->block);
	rank_size = (block_size / 8) + ((block_size % 8) ? 1 : 0) + 1;
	alloc_size += rank_size * sizeof(*rd->rank);
	rd = malloc(alloc_size);
	if (rd != NULL) {
		rd->size = size;
		rd->block_size = block_size;
		rd->rank_size = rank_size;
		rd->block = (uint32_t *)(void *)(rd + 1);
		rd->rank = (uint32_t *)(void *)(rd->block + block_size);
		rank = 0;
		for (i = 0; i < block_size; ++i) {
			if ((i % 8) == 0)
				rd->rank[i / 8] = rank;
			block = bs->block[i];
			rd->block[i] = block;
			rank += (size_t)popcount32(block);
		}
		rd->rank[rank_size - 1] = rank;
	}
	return rd;
}

int
rsdic_serialize(rsdic_t rd, FILE *fp)
{
	struct rsdic_file rdf;
	size_t block_size, i;
	uint32_t *block, buf[BUFSIZ];

	assert(rd != NULL);
	assert(fp != NULL);
	if (rd->size < 1 || rd->size > UINT64_MAX)
		return 1;
	rdf.size = htobe64((uint64_t)rd->size);
	if (fwrite(&rdf, sizeof(rdf), 1, fp) != 1)
		return 1;
	block_size = rd->block_size + rd->rank_size;
	block = rd->block;
	while (block_size > arraycount(buf)) {
		for (i = 0; i < arraycount(buf); ++i)
			buf[i] = htobe32(*block++);
		if (fwrite(&buf[0], sizeof(buf[0]), arraycount(buf), fp)
		  != arraycount(buf))
			return 1;
		block_size -= arraycount(buf);
	}
	for (i = 0; i < block_size; ++i)
		buf[i] = htobe32(*block++);
	if (fwrite(&buf[0], sizeof(buf[0]), block_size, fp) != block_size)
		return 1;
	return 0;
}

int
rsdic_store(rsdic_t rd, FILE *fp)
{
	struct rsdic_header rdh;

	assert(rd != NULL);
	assert(fp != NULL);
	memcpy(&rdh.magic[0],
	    RSDIC_HEADER_MAGIC, sizeof(rdh.magic));
	rdh.version = htobe32(RSDIC_HEADER_VERSION);
	if (fwrite(&rdh, sizeof(rdh), 1, fp) != 1)
		return 1;
	return rsdic_serialize(rd, fp);
}
