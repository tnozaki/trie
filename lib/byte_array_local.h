/*-
 * Copyright (c) 2018, 2021 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR LOCALIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * LOCALIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

#ifndef BYTE_ARRAY_LOCAL_H_
#define BYTE_ARRAY_LOCAL_H_

#include <stddef.h>
#include <stdint.h>

#define BYTE_ARRAY_HEADER_MAGIC		"byte_array"
#define BYTE_ARRAY_HEADER_VERSION	UINT32_C(1)

struct byte_array_header {
	char magic[sizeof(BYTE_ARRAY_HEADER_MAGIC) - 1];
	uint32_t version;
};

struct byte_array_file {
	uint64_t size;
};

struct byte_array_builder {
	char **bufp;
	size_t *sizep;
	size_t capacity;
};

#define BYTE_ARRAY_BUILDER_DEFAULT_CAPACITY	256
#define BYTE_ARRAY_BUILDER_MAX_CAPACITY		(SIZE_MAX/sizeof(char))

#endif /*!BYTE_ARRAY_LOCAL_H_*/
