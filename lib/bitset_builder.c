/*-
 * Copyright (c) 2018, 2021 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

#include "trie_defs.h"

#include "bitset_builder.h"
#include "bitset_local.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>

bitset_t
bitset_new(void)
{
	struct bitset *bs;

	bs = malloc(sizeof(*bs));
	if (bs != NULL) {
		bs->size = 0;
		bs->block_size = DEFAULT_BLOCK_SIZE;
		bs->block = calloc(bs->block_size, sizeof(*bs->block));
		if (bs->block == NULL) {
			free(bs);
			bs = NULL;
		}
	}
	return bs;
}

void
bitset_set_value(bitset_t bs, size_t pos, bool value)
{
	size_t i;
	uint32_t bits;

	assert(bs != NULL);
	i = pos / 32;
	bits = UINT32_C(1) << (pos % 32);
	if (value)
		bs->block[i] |= bits;
	else
		bs->block[i] &= ~bits;
}

int
bitset_push_back(bitset_t bs, bool value)
{
	size_t block_size;
	uint32_t *block;

	assert(bs != NULL);
	if (bs->size == bs->block_size * 32) {
		if (bs->block_size * 2 > SIZE_MAX / 32)
			return 1;
		block_size = bs->block_size * 2;
		block = realloc(bs->block, sizeof(*block) * block_size);
		if (block == NULL)
			return 1;
		memset(block + bs->block_size, 0, block_size - bs->block_size);
		bs->block_size = block_size;
		bs->block = block;
	}
	if (value)
		bs->block[bs->size / 32] |= (UINT32_C(1) << (bs->size % 32));
	++bs->size;
	return 0;
}

int
bitset_serialize(bitset_t bs, FILE *fp)
{
	struct bitset_file bsf;
	size_t block_size, i;
	uint32_t *block, buf[BUFSIZ];

	assert(bs != NULL);
	assert(fp != NULL);
	if (bs->size < 1 || bs->size > UINT64_MAX)
		return 1;
	bsf.size = htobe64((uint64_t)bs->size);
	if (fwrite(&bsf, sizeof(bsf), 1, fp) != 1)
		return 1;
	block_size = (bs->size / 32) + ((bs->size % 32) ? 1 : 0);
	block = bs->block;
	while (block_size > arraycount(buf)) {
		for (i = 0; i < arraycount(buf); ++i)
			buf[i] = htobe32(*block++);
		if (fwrite(&buf[0], sizeof(buf[0]),
		    arraycount(buf), fp) != arraycount(buf))
			return 1;
		block_size -= arraycount(buf);
	}
	for (i = 0; i < block_size; ++i)
		buf[i] = htobe32(*block++);
	if (fwrite(&buf[0], sizeof(buf[0]), block_size, fp) != block_size)
		return 1;
	return 0;
}

int
bitset_store(bitset_t bs, FILE *fp)
{
	struct bitset_header bsh;

	assert(bs != NULL);
	assert(fp != NULL);
	memcpy(&bsh.magic[0],
	    BITSET_HEADER_MAGIC, sizeof(bsh.magic));
	bsh.version = htobe32(BITSET_HEADER_VERSION);
	if (fwrite(&bsh, sizeof(bsh), 1, fp) != 1)
		return 1;
	return bitset_serialize(bs, fp);
}
