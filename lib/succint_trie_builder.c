/*-
 * Copyright (c) 2018, 2021 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

#include "trie_defs.h"

#include "bitset_builder.h"
#include "rsdic_builder.h"
#include "byte_array_builder.h"
#include "string_array_builder.h"
#include "succint_trie_builder.h"
#include "succint_trie_local.h"

#include <sys/queue.h>
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>

struct range {
	STAILQ_ENTRY(range) entry;
	size_t left, right;
};

static inline struct range *
range_new(size_t left, size_t right)
{
	struct range *r;
	r = malloc(sizeof(*r));
	if (r != NULL) {
		r->left = left;
		r->right = right;
	}
	return r;
}

static inline void
range_delete(struct range *r)
{
	free(r);
}

struct range_queue {
	STAILQ_HEAD(, range) q[2];
	bool i;
};

static inline void
range_queue_init(struct range_queue *rq)
{
	size_t i;
	for (i = 0; i < arraycount(rq->q); ++i)
		STAILQ_INIT(&rq->q[i]);
	rq->i = false;
}

static inline bool
range_queue_empty(struct range_queue *rq)
{
	return STAILQ_EMPTY(&rq->q[(size_t)rq->i]);
}

static inline void
range_queue_swap(struct range_queue *rq)
{
	rq->i = !rq->i;
}

static inline void
range_queue_pop(struct range_queue *rq, size_t *left, size_t *right)
{
	struct range *r;
	r = STAILQ_FIRST(&rq->q[(size_t)rq->i]);
	STAILQ_REMOVE_HEAD(&rq->q[(size_t)rq->i], entry);
	*left = r->left;
	*right = r->right;
	range_delete(r);
}

static inline int
range_queue_push(struct range_queue *rq, size_t left, size_t right)
{
	struct range *r;
	if ((r = range_new(left, right)) == NULL)
		return 1;
	STAILQ_INSERT_HEAD(&rq->q[(size_t)rq->i], r, entry);
	return 0;
}

static inline int
range_queue_push_next(struct range_queue *rq, size_t left, size_t right)
{
	struct range *r;
	if ((r = range_new(left, right)) == NULL)
		return 1;
	STAILQ_INSERT_HEAD(&rq->q[(size_t)!rq->i], r, entry);
	return 0;
}

static inline void
range_queue_uninit(struct range_queue *rq)
{
	size_t i;
	struct range *r;
	for (i = 0; i < arraycount(rq->q); ++i) {
		while (!STAILQ_EMPTY(&rq->q[i])) {
			r = STAILQ_FIRST(&rq->q[i]);
			STAILQ_REMOVE_HEAD(&rq->q[i], entry);
			range_delete(r);
		}
	}
}

struct succint_trie_builder {
	bitset_t loud, term, tail;
	string_array_builder_t vtails_sab;
	byte_array_builder_t edges_bab;
};

static inline int
succint_trie_builder_init(struct succint_trie_builder *stb, succint_trie_t st)
{
	memset(stb, 0, sizeof(*stb));
	stb->loud = bitset_new();
	if (stb->loud == NULL)
		return 1;
	if (bitset_push_back(stb->loud, false))
		return 1;
	if (bitset_push_back(stb->loud, true))
		return 1;
	stb->term = bitset_new();
	if (stb->term == NULL)
		return 1;
	stb->tail = bitset_new();
	if (stb->tail == NULL)
		return 1;
	stb->vtails_sab = string_array_builder_new(
	    (const char ***)&st->vtails, &st->vtails_size);
	if (stb->vtails_sab == NULL)
		return 1;
	stb->edges_bab = byte_array_builder_new(
	    &st->edges, &st->edges_size);
	if (stb->edges_bab == NULL)
		return 1;
	return 0;
}

static inline void
succint_trie_builder_uninit(struct succint_trie_builder *stb)
{
	size_t i;

	if (stb->loud != NULL)
		bitset_delete(stb->loud);
	if (stb->term != NULL)
		bitset_delete(stb->term);
	if (stb->tail != NULL)
		bitset_delete(stb->tail);
	if (stb->vtails_sab != NULL)
		string_array_builder_delete(stb->vtails_sab);
	if (stb->edges_bab != NULL)
		byte_array_builder_delete(stb->edges_bab);
}

static inline int
sccint_trie_build(succint_trie_t st, const char **keys, size_t key_num)
{
	struct range_queue rq;
	struct succint_trie_builder stb;
	size_t depth, left, right, cur_size, newleft;
	const char *cur;
	char *tail;
	int c;
	bool term;

	range_queue_init(&rq);
	if (succint_trie_builder_init(&stb, st))
		goto fatal;
	if (key_num > 0 && range_queue_push(&rq, 0, key_num))
		goto fatal;
	for (depth = 0;;) {
		if (range_queue_empty(&rq)) {
			range_queue_swap(&rq);
			if (range_queue_empty(&rq))
				break;
			++depth;
		}
		range_queue_pop(&rq, &left, &right);
		cur = keys[left];
		cur_size = strlen(cur);
		newleft = left + 1;
		if (newleft == right && depth + 1 < cur_size) {
			if (bitset_push_back(stb.loud, true)
			 || bitset_push_back(stb.term, true)
			 || bitset_push_back(stb.tail, true)
			 || (tail = strdup(&cur[depth])) == NULL
			 || string_array_builder_append(stb.vtails_sab,
			        (const char *)tail) == NULL)
				goto fatal;
			continue;
		}
		if (bitset_push_back(stb.tail, false))
			goto fatal;
		assert(left < key_num);
		term = depth == cur_size;
		if (bitset_push_back(stb.term, term))
			goto fatal;
		if (term && newleft == right) {
			if (bitset_push_back(stb.loud, true))
				goto fatal;
			continue;
		}
		for (;;) {
			assert(strlen(keys[left]) > depth);
			c = (unsigned char)keys[left][depth];
			while (newleft < right && c == keys[newleft][depth])
				++newleft;
			if (byte_array_builder_append(stb.edges_bab, c) == EOF
                         || bitset_push_back(stb.loud, false))
				goto fatal;
			if (range_queue_push_next(&rq, left, newleft))
				goto fatal;
			if (newleft == right)
				break;
			left = newleft;
		}
		if (bitset_push_back(stb.loud, true))
			goto fatal;
	}
	st->key_num = key_num;
	if ((st->loud = rsdic_new(stb.loud)) == NULL
         || (st->term = rsdic_new(stb.term)) == NULL
         || (st->tail = rsdic_new(stb.tail)) == NULL
	 || string_array_builder_shrink_to_fit(stb.vtails_sab) == EOF
	 || byte_array_builder_shrink_to_fit(stb.edges_bab) == EOF)
		goto fatal;
	succint_trie_builder_uninit(&stb);
	return 0;
fatal:
	range_queue_uninit(&rq);
	succint_trie_builder_uninit(&stb);
	return 1;
}

succint_trie_t
succint_trie_new(const char **keys, size_t key_num)
{
	succint_trie_t st;

	st = malloc(sizeof(*st));
	if (st != NULL) {
		memset(st, 0, sizeof(st));
		if (sccint_trie_build(st, keys, key_num)) {
			succint_trie_delete(st);
			st = NULL;
		}
	}
	return st;
}

int
succint_trie_serialize(succint_trie_t st, FILE *fp)
{
	struct succint_trie_file stf;

	assert(st != NULL);
	assert(fp != NULL);
	if (st->key_num < 1 || st->key_num > UINT64_MAX)
		return 1;
	stf.key_num = htobe64((uint64_t)st->key_num);
	if (fwrite(&stf, sizeof(stf), 1, fp) != 1)
		return 1;
	if (rsdic_serialize(st->loud, fp)
	 || rsdic_serialize(st->term, fp)
	 || rsdic_serialize(st->tail, fp)
	 || string_array_serialize((const char **)
	        st->vtails, st->vtails_size, fp)
	 || byte_array_serialize((const char *)
	        st->edges, st->edges_size, fp))
		return 1;
	return 0;
}

int
succint_trie_store(succint_trie_t st, FILE *fp)
{
	struct succint_trie_header sth;

	assert(st != NULL);
	assert(fp != NULL);
	memcpy(&sth.magic[0],
	    SUCCINT_TRIE_HEADER_MAGIC, sizeof(sth.magic));
	sth.version = htobe32(SUCCINT_TRIE_HEADER_VERSION);
	if (fwrite(&sth, sizeof(sth), 1, fp) != 1)
		return 1;
	return succint_trie_serialize(st, fp);
}
