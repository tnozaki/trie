/*-
 * Copyright (c) 2018, 2021 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

#include "trie_defs.h"

#include "string_array.h"
#include "string_array_local.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>

int
string_array_deserialize(FILE *fp, char ***datap, size_t *sizep)
{
	struct string_array_file saf;
	uint64_t u64;
	char **data, *s;
	size_t size, i, n;

	assert(fp != NULL);
	assert(datap != NULL);
	assert(sizep != NULL);
	if (fread(&saf, sizeof(saf), 1, fp) != 1)
		return 1;
	saf.size = be64toh(saf.size);
	if (saf.size < 1 || saf.size > SIZE_MAX)
		return 1;
	size = (size_t)saf.size;
	data = malloc(size * sizeof(*data));
	if (data == NULL)
		return 1;
	for (i = 0; i < size; ++i) {
		if (fread(&u64, sizeof(u64), 1, fp) != 1)
			goto fatal;
		u64 = be64toh(u64);
		if (u64 > SIZE_MAX)
			goto fatal;
		n = (size_t)u64;
		s = malloc((n + 1) * sizeof(*s));
		if (s == NULL)
			goto fatal;
		if (fread(s, sizeof(*s), n, fp) != n) {
			free(s);
			goto fatal;
		}
		s[n] = '\0';
		data[i] = s;
	}
	*datap = data;
	*sizep = size;
	return 0;
fatal:
	while (i > 1)
		free(data[--i]);
	free(data);
	return 1;
}

int
string_array_load(FILE *fp, char ***datap, size_t *sizep)
{
	struct string_array_header sah;

	assert(fp != NULL);
	assert(datap != NULL);
	assert(sizep != NULL);
	if (fread(&sah, sizeof(sah), 1, fp) != 1)
		return 1;
	if (memcmp(&sah.magic[0],
	    STRING_ARRAY_HEADER_MAGIC, sizeof(sah.magic)) != 0)
		return 1;
	if (be32toh(sah.version) != STRING_ARRAY_HEADER_VERSION)
		return 1;
	return string_array_deserialize(fp, datap, sizep);
}
